CREATE DATABASE  IF NOT EXISTS `arsam` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `arsam`;
-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: arsam
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Categoria`
--

DROP TABLE IF EXISTS `Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `urlLogo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categoria`
--

LOCK TABLES `Categoria` WRITE;
/*!40000 ALTER TABLE `Categoria` DISABLE KEYS */;
INSERT INTO `Categoria` VALUES (1,'Orçamento (Execução)','/arsam/img/fiscal.png'),(2,'Relatórios anuais','/arsam/img/contratos.png'),(3,'Contratos','/arsam/img/contratos.png'),(4,'Despesas','/arsam/img/despesas.png'),(5,'Diárias e passagens','/arsam/img/passagens.png'),(6,'Licitações','/arsam/img/licitacoes.png'),(7,'Pessoal','/arsam/img/servidores.png'),(8,'Receitas','/arsam/img/receitas.png'),(9,'Balanço geral','/arsam/img/balanco.png');
/*!40000 ALTER TABLE `Categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lancamento`
--

DROP TABLE IF EXISTS `Lancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lancamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `ano` int(11) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lancamento_categoria_idx` (`idcategoria`),
  CONSTRAINT `fk_lancamento_categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lancamento`
--

LOCK TABLES `Lancamento` WRITE;
/*!40000 ALTER TABLE `Lancamento` DISABLE KEYS */;
INSERT INTO `Lancamento` VALUES (1,1,2016,1,'Orçamento Jan/2016','/arsam/transparencia/orcamento_2016_01.pdf'),(2,1,2016,2,'Orçamento Fev/2016','/arsam/transparencia/orcamento_2016_02.pdf'),(3,1,2016,3,'Orçamento Mar/2016','/arsam/transparencia/orcamento_2016_03.pdf'),(4,1,2016,4,'Orçamento Abr/2016','/arsam/transparencia/orcamento_2016_04.pdf'),(5,1,2016,5,'Orçamento Mai/2016','/arsam/transparencia/orcamento_2016_05.pdf'),(6,1,2016,6,'Orçamento Jun/2016','/arsam/transparencia/orcamento_2016_06.pdf'),(7,1,2016,7,'Orçamento Jul/2016','/arsam/transparencia/orcamento_2016_07.pdf'),(8,1,2016,8,'Orçamento Ago/2016','/arsam/transparencia/orcamento_2016_08.pdf'),(9,1,2016,9,'Orçamento Set/2016','/arsam/transparencia/orcamento_2016_09.pdf'),(10,1,2016,10,'Orçamento Out/2016','/arsam/transparencia/orcamento_2016_10.pdf'),(11,1,2016,11,'Orçamento Nov/2016','/arsam/transparencia/orcamento_2016_11.pdf'),(12,1,2015,1,'Orçamento Jan/2015','/arsam/transparencia/orcamento_2015_01.pdf'),(13,1,2015,2,'Orçamento Fev/2015','/arsam/transparencia/orcamento_2015_02.pdf'),(14,1,2015,3,'Orçamento Mar/2015 UG 11209','/arsam/transparencia/orcamento_2015_03_1.pdf'),(15,1,2015,3,'Orçamento Mar/2015','/arsam/transparencia/orcamento_2015_03_2.pdf'),(16,1,2015,4,'Orçamento Abr/2015 UG 11209','/arsam/transparencia/orcamento_2015_04_1.pdf'),(17,1,2015,4,'Orçamento Abr/2015','/arsam/transparencia/orcamento_2015_04_2.pdf'),(18,1,2015,5,'Orçamento Mai/2015 UG 11209','/arsam/transparencia/orcamento_2015_05_1.pdf'),(19,1,2015,5,'Orçamento Mai/2015','/arsam/transparencia/orcamento_2015_05_2.pdf'),(20,1,2015,6,'Orçamento Jun/2015','/arsam/transparencia/orcamento_2015_06.pdf'),(21,1,2015,7,'Orçamento Jul/2015','/arsam/transparencia/orcamento_2015_07.pdf'),(22,1,2015,8,'Orçamento Ago/2015','/arsam/transparencia/orcamento_2015_08.pdf'),(23,1,2015,9,'Orçamento Set/2015','/arsam/transparencia/orcamento_2015_09.pdf'),(24,1,2015,10,'Orçamento Out/2015','/arsam/transparencia/orcamento_2015_10.pdf'),(25,1,2015,11,'Orçamento Nov/2015','/arsam/transparencia/orcamento_2015_11.pdf'),(26,1,2015,12,'Orçamento Dez/2015','/arsam/transparencia/orcamento_2015_12.pdf'),(27,2,2015,1,'RELATÓRIO de 2015','/arsam/transparencia/relatorio_2015.pdf'),(28,2,2014,1,'RELATÓRIO de 2014','/arsam/transparencia/relatorio_2014.pdf'),(29,2,2010,1,'RELATÓRIO de 2010','/arsam/transparencia/relatorio_2010.pdf'),(30,2,2007,1,'RELATÓRIO de 2007','/arsam/transparencia/relatorio_2007.pdf'),(31,2,2006,1,'RELATÓRIO de 2006','/arsam/transparencia/relatorio_2006.pdf'),(32,3,2016,6,'Contratos de Jun/2016','/arsam/transparencia/contrato_2016_06_1.pdf'),(33,3,2016,6,'Contratos de Jun/2016','/arsam/transparencia/contrato_2016_06_2.pdf'),(34,3,2016,6,'Contratos de Jun/2016','/arsam/transparencia/contrato_2016_06_3.pdf'),(35,3,2016,6,'Contratos de Jun/2016','/arsam/transparencia/contrato_2016_06_4.pdf'),(36,3,2016,6,'Contratos de Jun/2016','/arsam/transparencia/contrato_2016_06_5.pdf'),(37,3,2016,6,'Contratos de Jun/2016','/arsam/transparencia/contrato_2016_06_6.pdf'),(38,3,2016,7,'PDF   PRINCIPAIS PONTOS DA LEI DE ACESSO A INFORMAÇÕES PÚBLICAS','/arsam/transparencia/contrato_2016_07.pdf'),(39,3,2015,6,'Atestado Médico de Jun/2015','/arsam/transparencia/contrato_2015_06_1.pdf'),(40,3,2015,6,'Atestado Médico de Jun/2015','/arsam/transparencia/contrato_2015_06_2.pdf'),(41,3,2015,6,'Atestado Médico de Jun/2015','/arsam/transparencia/contrato_2015_06_3.pdf'),(42,3,2015,6,'Atestado Médico de Jun/2015','/arsam/transparencia/contrato_2015_06_4.pdf'),(43,3,2015,6,'Atestado Médico de Jun/2015','/arsam/transparencia/contrato_2015_06_5.pdf'),(44,3,2015,6,'Atestado Médico de Jun/2015','/arsam/transparencia/contrato_2015_06_6.pdf'),(45,3,2014,6,'Atestado Médico de Jun/2014','/arsam/transparencia/contrato_2014_06_1.pdf'),(46,3,2014,6,'Atestado Médico de Jun/2014','/arsam/transparencia/contrato_2014_06_2.pdf'),(47,3,2014,6,'Atestado Médico de Jun/2014','/arsam/transparencia/contrato_2014_06_3.pdf'),(48,3,2014,6,'Atestado Médico de Jun/2014','/arsam/transparencia/contrato_2014_06_4.pdf'),(49,3,2014,6,'Atestado Médico de Jun/2014','/arsam/transparencia/contrato_2014_06_5.pdf'),(50,3,2014,6,'Atestado Médico de Jun/2014','/arsam/transparencia/contrato_2014_06_6.pdf'),(51,3,2013,6,'Atestado Médico de Jun/2013','/arsam/transparencia/contrato_2013_06_1.pdf'),(52,3,2013,6,'Atestado Médico de Jun/2013','/arsam/transparencia/contrato_2013_06_2.pdf'),(53,3,2013,6,'Atestado Médico de Jun/2013','/arsam/transparencia/contrato_2013_06_3.pdf'),(54,3,2013,6,'Atestado Médico de Jun/2013','/arsam/transparencia/contrato_2013_06_4.pdf'),(55,3,2013,6,'Atestado Médico de Jun/2013','/arsam/transparencia/contrato_2013_06_5.pdf'),(56,3,2013,6,'Atestado Médico de Jun/2013','/arsam/transparencia/contrato_2013_06_6.pdf'),(57,3,2012,6,'Atestado Médico de Jun/2012','/arsam/transparencia/contrato_2012_06_1.pdf'),(58,3,2012,6,'Atestado Médico de Jun/2012','/arsam/transparencia/contrato_2012_06_2.pdf'),(59,3,2012,6,'Atestado Médico de Jun/2012','/arsam/transparencia/contrato_2012_06_3.pdf'),(60,3,2012,6,'Atestado Médico de Jun/2012','/arsam/transparencia/contrato_2012_06_4.pdf'),(61,3,2012,6,'Atestado Médico de Jun/2012','/arsam/transparencia/contrato_2012_06_5.pdf'),(62,3,2012,6,'Atestado Médico de Jun/2012','/arsam/transparencia/contrato_2012_06_6.pdf'),(63,4,2016,1,'Despesas de  Jan/2016','/arsam/transparencia/despesas_2016_01.pdf'),(64,4,2016,2,'Despesas de  Fev/2016','/arsam/transparencia/despesas_2016_02.pdf'),(65,4,2016,3,'Despesas de  Mar/2016','/arsam/transparencia/despesas_2016_03.pdf'),(66,4,2016,4,'Despesas de  Abr/2016','/arsam/transparencia/despesas_2016_04.pdf'),(67,4,2016,5,'Despesas de  Mai/2016','/arsam/transparencia/despesas_2016_05.pdf'),(68,4,2016,6,'Despesas de  Jun/2016','/arsam/transparencia/despesas_2016_06.pdf'),(69,4,2016,7,'Despesas de  Jul/2016','/arsam/transparencia/despesas_2016_07.pdf'),(70,4,2016,8,'Despesas de  Ago/2016','/arsam/transparencia/despesas_2016_08.pdf'),(71,4,2016,9,'Despesas de  Set/2016','/arsam/transparencia/despesas_2016_09.pdf'),(72,4,2016,10,'Despesas de  Out/2016','/arsam/transparencia/despesas_2016_10.pdf'),(73,4,2016,11,'Despesas de  Nov/2016','/arsam/transparencia/despesas_2016_11.pdf'),(74,4,2015,1,'Despesas de  Jan/2015','/arsam/transparencia/despesas_2015_01.pdf'),(75,4,2015,2,'Despesas de  Fev/2015','/arsam/transparencia/despesas_2015_02.pdf'),(76,4,2015,3,'Despesas de  Mar/2015','/arsam/transparencia/despesas_2015_03.pdf'),(77,4,2015,4,'Despesas de  Abr/2015 UG 11209','/arsam/transparencia/despesas_2015_04_1.pdf'),(78,4,2015,4,'Despesas de  Abr/2015','/arsam/transparencia/despesas_2015_04_2.pdf'),(79,4,2015,5,'Despesas de  Mai/2015','/arsam/transparencia/despesas_2015_05.pdf'),(80,4,2015,6,'Despesas de  Jun/2015','/arsam/transparencia/despesas_2015_06.pdf'),(81,4,2015,7,'Despesas de  Jul/2015','/arsam/transparencia/despesas_2015_07.pdf'),(82,4,2015,8,'Despesas de  Ago/2015','/arsam/transparencia/despesas_2015_08.pdf'),(83,4,2015,9,'Despesas de  Set/2015','/arsam/transparencia/despesas_2015_09.pdf'),(84,4,2015,10,'Despesas de  Out/2015','/arsam/transparencia/despesas_2015_10.pdf'),(85,4,2015,11,'Despesas de  Nov/2015','/arsam/transparencia/despesas_2015_11.pdf'),(86,4,2015,12,'Despesas de  Dez/2015','/arsam/transparencia/despesas_2015_12.pdf'),(87,4,2014,6,'Atestado Médico Jun/2014','/arsam/transparencia/despesas_2014_06_1.pdf'),(88,4,2014,6,'Atestado Médico Jun/2014','/arsam/transparencia/despesas_2014_06_2.pdf'),(89,4,2014,6,'Atestado Médico Jun/2014','/arsam/transparencia/despesas_2014_06_3.pdf'),(90,4,2014,6,'Atestado Médico Jun/2014','/arsam/transparencia/despesas_2014_06_4.pdf'),(91,4,2014,6,'Atestado Médico Jun/2014','/arsam/transparencia/despesas_2014_06_5.pdf'),(92,4,2014,6,'Atestado Médico Jun/2014','/arsam/transparencia/despesas_2014_06_6.pdf'),(93,4,2013,6,'Atestado Médico Jun/2013','/arsam/transparencia/despesas_2013_06_1.pdf'),(94,4,2013,6,'Atestado Médico Jun/2013','/arsam/transparencia/despesas_2013_06_2.pdf'),(95,4,2013,6,'Atestado Médico Jun/2013','/arsam/transparencia/despesas_2013_06_3.pdf'),(96,4,2013,6,'Atestado Médico Jun/2013','/arsam/transparencia/despesas_2013_06_4.pdf'),(97,4,2013,6,'Atestado Médico Jun/2013','/arsam/transparencia/despesas_2013_06_5.pdf'),(98,4,2013,6,'Atestado Médico Jun/2013','/arsam/transparencia/despesas_2013_06_6.pdf'),(99,4,2012,6,'Atestado Médico Jun/2012','/arsam/transparencia/despesas_2012_06_1.pdf'),(100,4,2012,6,'Atestado Médico Jun/2012','/arsam/transparencia/despesas_2012_06_2.pdf'),(101,4,2012,6,'Atestado Médico Jun/2012','/arsam/transparencia/despesas_2012_06_3.pdf'),(102,4,2012,6,'Atestado Médico Jun/2012','/arsam/transparencia/despesas_2012_06_4.pdf'),(103,4,2012,6,'Atestado Médico Jun/2012','/arsam/transparencia/despesas_2012_06_5.pdf'),(104,4,2012,6,'Atestado Médico Jun/2012','/arsam/transparencia/despesas_2012_06_6.pdf'),(105,4,2015,1,'Despesas de Jan/2015','/arsam/transparencia/despesas_2015_01.pdf'),(106,4,2015,2,'Despesas de Fev/2015','/arsam/transparencia/despesas_2015_02.pdf'),(107,4,2015,3,'Despesas de Mar/2015','/arsam/transparencia/despesas_2015_03.pdf'),(108,4,2015,4,'Despesas de Abri/2015 1','/arsam/transparencia/despesas_2015_04_1.pdf'),(109,4,2015,4,'Despesas de Abri/2015 2','/arsam/transparencia/despesas_2015_04_2.pdf'),(110,4,2015,5,'Despesas de Mai/2015','/arsam/transparencia/despesas_2015_05.pdf'),(111,4,2015,6,'Despesas de Jun/2015','/arsam/transparencia/despesas_2015_06.pdf'),(112,4,2015,7,'Despesas de Jul/2015','/arsam/transparencia/despesas_2015_07.pdf'),(113,4,2015,8,'Despesas de Ago/2015','/arsam/transparencia/despesas_2015_08.pdf'),(114,4,2015,9,'Despesas de Set/2015','/arsam/transparencia/despesas_2015_09.pdf'),(115,4,2015,10,'Despesas de Out/2015','/arsam/transparencia/despesas_2015_10.pdf'),(116,4,2015,11,'Despesas de Nov/2015','/arsam/transparencia/despesas_2015_11.pdf'),(117,4,2015,12,'Despesas de Dez/2015','/arsam/transparencia/despesas_2015_12.pdf'),(118,9,2015,1,'01 Prestação de Contas de 2015 UG - 11209 - TCE-1-52','/arsam/transparencia/prestacaocontas_2015_11209_1.pdf'),(119,9,2015,1,'01 Prestação de Contas de 2015 UG - 11209 - TCE-53-105','/arsam/transparencia/prestacaocontas_2015_11209_2.pdf'),(120,9,2015,1,'01 Prestação de Contas de 2015 UG - 11209 - TCE-106-158','/arsam/transparencia/prestacaocontas_2015_11209_3.pdf'),(121,9,2015,1,'01 Prestação de Contas de 2015 UG - 11209 - TCE-159-211','/arsam/transparencia/prestacaocontas_2015_11209_4.pdf'),(122,9,2015,1,'01 Prestação de Contas de 2015 UG - 11209 - TCE-211-260','/arsam/transparencia/prestacaocontas_2015_11209_5.pdf'),(123,9,2015,2,'02 Prestação de Contas de 2015 UG - 25201 - 1-50','/arsam/transparencia/prestacaocontas_2015_25201_1.pdf'),(124,9,2015,2,'02 Prestação de Contas de 2015 UG - 25201 - 51-100','/arsam/transparencia/prestacaocontas_2015_25201_2.pdf'),(125,9,2015,2,'02 Prestação de Contas de 2015 UG - 25201 - 101-150','/arsam/transparencia/prestacaocontas_2015_25201_3.pdf'),(126,9,2015,2,'02 Prestação de Contas de 2015 UG - 25201 - 151-200','/arsam/transparencia/prestacaocontas_2015_25201_4.pdf'),(127,9,2015,2,'02 Prestação de Contas de 2015 UG - 25201 - 201-249','/arsam/transparencia/prestacaocontas_2015_25201_5.pdf'),(128,8,2015,1,'Receita de Jan/2015','/arsam/transparencia/receita_2015_01.pdf'),(129,8,2015,2,'Receita de Fev/2015','/arsam/transparencia/receita_2015_02.pdf'),(130,8,2015,3,'Receita de Mar/2015','/arsam/transparencia/receita_2015_03.pdf'),(131,8,2015,4,'Receita de Abr/2015','/arsam/transparencia/receita_2015_04.pdf'),(132,8,2015,5,'Receita de Mai/2015','/arsam/transparencia/receita_2015_05.pdf'),(133,8,2015,6,'Receita de Jun/2015','/arsam/transparencia/receita_2015_06.pdf'),(134,8,2015,7,'Receita de Jul/2015','/arsam/transparencia/receita_2015_07.pdf'),(135,8,2015,8,'Receita de Ago/2015','/arsam/transparencia/receita_2015_08.pdf'),(136,8,2015,9,'Receita de Set/2015','/arsam/transparencia/receita_2015_09.pdf'),(137,8,2015,10,'Receita de Out/2015','/arsam/transparencia/receita_2015_10.pdf'),(138,8,2015,11,'Receita de Nov/2015','/arsam/transparencia/receita_2015_11.pdf'),(139,8,2015,12,'Receita de Dez/2015','/arsam/transparencia/receita_2015_12.pdf'),(140,8,2016,1,'Receita de Jan/2016','/arsam/transparencia/receita_2016_01.pdf'),(141,8,2016,2,'Receita de Fev/2016','/arsam/transparencia/receita_2016_02.pdf'),(142,8,2016,3,'Receita de Mar/2016','/arsam/transparencia/receita_2016_03.pdf'),(143,8,2016,4,'Receita de Abr/2016','/arsam/transparencia/receita_2016_04.pdf'),(144,8,2016,5,'Receita de Mai/2016','/arsam/transparencia/receita_2016_05.pdf'),(145,8,2016,6,'Receita de Jun/2016','/arsam/transparencia/receita_2016_06.pdf'),(146,8,2016,7,'Receita de Jul/2016','/arsam/transparencia/receita_2016_07.pdf'),(147,8,2016,8,'Receita de Ago/2016','/arsam/transparencia/receita_2016_08.pdf'),(148,8,2016,9,'Receita de Set/2016','/arsam/transparencia/receita_2016_09.pdf'),(149,8,2016,10,'Receita de Out/2016','/arsam/transparencia/receita_2016_10.pdf'),(150,8,2016,11,'Receita de Nov/2016','/arsam/transparencia/receita_2016_11.pdf');
/*!40000 ALTER TABLE `Lancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigoPais` varchar(255) DEFAULT NULL,
  `firebaseId` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-05 10:34:40
